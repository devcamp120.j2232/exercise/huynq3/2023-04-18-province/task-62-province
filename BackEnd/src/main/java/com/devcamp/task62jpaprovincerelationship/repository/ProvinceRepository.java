package com.devcamp.task62jpaprovincerelationship.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62jpaprovincerelationship.models.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer> {
    Province findById(int id);
}
