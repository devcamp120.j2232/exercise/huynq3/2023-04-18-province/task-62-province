package com.devcamp.task62jpaprovincerelationship.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task62jpaprovincerelationship.models.Ward;
import com.devcamp.task62jpaprovincerelationship.repository.WardRepository;

@Service
public class WardService {
    
    @Autowired
    WardRepository wardRepository;

    public ArrayList<Ward> getAllWards(){
        ArrayList<Ward> listWards = new ArrayList<>();
        wardRepository.findAll().forEach(listWards::add);
        return listWards;
    }
}

